Copyright (C) 2011 Massachusetts Institute of Technology
This is free software; see the source for copying conditions. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Image saved on Thursday May 3, 2018 at 3:34:25 PM
  Release 9.1.1     || Microcode 15.3 || Runtime 15.7 || SF 4.41
  LIAR/x86-64 4.118 || Edwin 3.116
;You are in an interaction window of the Edwin editor.
;Type `C-h' for help, or `C-h t' for a tutorial.
;`C-h m' will describe some commands.
;`C-h' means: hold down the Ctrl key and type `h'.
;Package: (user)

(+ 1 5)
;Value: 6

(+1 10)
;The object 1 is not applicable.
;To continue, call RESTART with an option number:
; (RESTART 2) => Specify a procedure to use in its place.
; (RESTART 1) => Return to read-eval-print level 1.
;Start debugger? (y or n): n

(+ 1 10)
;Value: 11

486
;Value: 486

(+ 137 349)
;Value: 486

(-1000 334)
;The object -1000 is not applicable.
;To continue, call RESTART with an option number:
; (RESTART 3) => Specify a procedure to use in its place.
; (RESTART 2) => Return to read-eval-print level 2.
; (RESTART 1) => Return to read-eval-print level 1.
;Start debugger? (y or n): n

(- 1000 334)
;Value: 666

(* 5 99)
;Value: 495

(/ 10 5)
;Value: 2

(+ 2.7 10)
;Value: 12.7

(+ 21 35 12 7)
;Value: 75

(* 25 4 12)
;Value: 1200

(+ (* 3 5) (- 10 6))
;Value: 19

(+ (* 3 (+ (* 2 4) (+ 3 5))) (+ (- 10 7) 6))
;Value: 57


(+ (* 3 
      (+ (* 2 4) 
	 (+ 3 5))) 
   (+ (- 10 7)
      6))
;Value: 57

(+ (* 3 
      (+ (* 2 
	    4) 
	 (+ 3 
	    5))) 
   (+ (- 10 
	 7) 
      6))
;Value: 57

(define size 2)

size
;Unbound variable: size
;To continue, call RESTART with an option number:
; (RESTART 5) => Specify a value to use instead of size.
; (RESTART 4) => Define size to a given value.
; (RESTART 3) => Return to read-eval-print level 3.
; (RESTART 2) => Return to read-eval-print level 2.
; (RESTART 1) => Return to read-eval-print level 1.
;Start debugger? (y or n): n

(define size 2)
;Value: size

size
;Value: 2

(* 5 size)
;Value: 10

(define pi 3.14159)
;Value: pi

(define radius 10)
;Value: radius

(* pi (* radius radius))
;Value: 314.159

(define circumference (* 2 pi radius))
;Value: circumference

circumference
;Value: 62.8318

(define (square x) (* x x))
;Value: square

(square 21)
;Value: 441


(square (+ 2 5))
;Value: 49

(square ( square 3))
;Value: 81

(define (sum-of-squares x y ) (+ (square x) (square y)))
;Value: sum-of-squares

(sum-of-squares 3 4)
;Value: 25

(define (f a)
  (sum-of-squares (+ a 1) (* a 2)))
;Value: f

(f 5)
;Value: 136

10
;Value: 10

(+ 5 3 4)
;Value: 12

(- 9 1)
;Value: 8

(/ 6 2)
;Value: 3

(+ (* 2 4) (- 4 6))
;Value: 6

(define a 3)
;Value: a

(define b (+ a 1))
;Value: b

a
;Value: 3

b
;Value: 4

(+ a b (* a b))
;Value: 19

(= a b)
;Value: #f

(if (and (> b a) (< b (* a b)))
    b
    a)
;Value: 4

(cond ((= a 4) 6)
      ((= b 4) (+ 6 7 a))
      (else 25))
;Value: 16

(+ 2 (if (> b a) b a))
;Value: 6

(* (cond ((> a b) a)
	 ((< a b) b)
	 (else -1))
   (+ a 1))
;Value: 16

(/ (+ 5 4 (- 2 (- 3 (+ 6 (/ 4 5)))))
   (* 3
      (- 6 2)
      (- 2 7)))
;Value: -37/150

sum-of-squares

(define (g x y z)
  (cond ((< x (and y z)) (sum-of-squares y z))
	((< y (and x z)) (sum-of-squares x z))
	((< z (and x y)) (sum-of-squares x z))
	((> x (and y z)) (sum-of-squares y x))
	((> y (and x z)) (sum-of-squares y z))
	((< z (and y x)) (sum-of-squares y z))
	((= x (and y z)) (sum-of-squares y z))))
;Value: g

(g 2 3 4)
;Value: 25

(g 2 2 3)
;Value: 13

(g 1 1 1)
;Value: 2

(define (abs x)
  (if (< x 0)
      (- x)
      x))
;Value: abs

(abs -2)
;Value: 2

(abs 2)
;Value: 2

(define (sqrt-iter guess x)
  (if (good-enough? guess x)
      guess
      (sqrt-iter (improve guess x) x)))
;Value: sqrt-iter

(define (improve guess x)
  (average guess (/ x guess)))
;Value: improve

(define (average x y)
  (/ (+ x y) 2))
;Value: average

(define (good-enough? guess x)
  (< (abs (- (square guess) x)) 0.001))
;Value: good-enough?


(define (sqrt x)
  (sqrt-iter 1.0 x))
;Value: sqrt


(sqrt 9)
;Value: 3.00009155413138

(sqrt (+ 100 37))
;Value: 11.704699917758145

(sqrt (+ (sqrt 2) (sqrt 3)))
;Value: 1.7739279023207892

(sqrt (sqrt 1000))
;Value: 5.623413773109398

(square (sqrt 1000))
;Value: 1000.000369924366

(define (new-if predicate
		then-clause
		else-clause)
  (cond (predicate then-clause)
	(else else-clause)))
;Value: new-if

(new-if (= 2 3) 0 5)
;Value: 5

(new-if (= 1 1) 0 5)
;Value: 0

(define (new-sqrt-iter guess x)
  (new-if (good-enough? guess x)
	  guess
	  (new-sqrt-iter (improve guess x) x)))
;Value: new-sqrt-iter

(new-sqrt-iter 1.0 9)
;Aborting!: maximum recursion depth exceeded
;Start debugger? (y or n): y
;Starting debugger...




(sqrt 0.001)
;Value: .04124542607499115

(sqrt 0.0001)
;Value: .03230844833048122

(square .03230844833048122)
;Value: 1.0438358335233748e-3

(sqrt 100)
;Value: 316.2277660203896

(sqrt 100)
;Value: 10.000000000139897



(+ 1 1)
;Value: 2

(define (good-enough? guess x)
  (< (/ (abs (- guess (improve-cube guess x)))
	guess)
     0.00000001))
;Value: good-enough?

;Value: good-enough?

;Value: good-enough?

;Value: good-enough?

;Value: good-enough?

(sqrt .001)
;Value: .03162277660168433

;Value: .03162278245070105

;Value: .03162278245070105

;Value: .03162278245070105


(sqrt 10000000000)
;Value: 100000.00015603233

(define (cube x)
  (* x x x))
;Value: cube


(define (improve-cube guess x)
  (/ 
   (+ 
    (/ x 
       (square guess)) 
    (* 2 guess))
   3))
;Value: improve-cube


(define (cube-iter guess x)
 (if (good-enough? guess x)
     guess
     (cube-iter (improve-cube guess x)
		x)))
;Value: cube-iter

(define (cube-rt x)
  (cube-iter 1.0 x))
;Value: cube-rt

;Value: cube-rt

(improve-cube 2.015250336039381 8)
;Value: 2.000115115270362

;Value: 2.015250336039381

;Value: 

;Value: 904166765387115027182834/451375287904458254799225

;Value: 107796982/51792075

;Value: 554/225

;Value: 10/3

(/ 904166765387115027182834.0 451375287904458254799225)
;Value: 2.003137499141287


(cube-rt 8)
;Value: 2.000000000012062


(cube-rt 16)
;Value: 2.5198420997976982

(cube-rt 27)
;Value: 3.0000000000000977

(cube 4)
;Value: 64

(cube-rt 64)
;Value: 4.000000000076121

(cube 235)
;Value: 12977875

(cube-rt 12977875)
;Value: 235.00000000619585

(+ 1 2)
;Value: 3

(define (ack x y)
  (cond ((= y 0) 0)
	((= x 0) (* 2 y))
	((= y 1) 2)
	(else (ack (- x 1)
		   (ack x (- y 1))))))
;Value: ack

(ack 1 10)

(= 10 0)
;Value: #f

(= 1 0)
;Value: #f

(= 10 1)
;Value: #f

(ack (- 1 1)
     (ack 1 (- 10 1)))
;Value: 1024

(ack 1 10)
;Value: 1024

(ack 1 9)

(ack 1 8)

(ack 1 7)

(ack 1 6)

(ack 1 5)

(ack 1 4)

(ack 1 3)

(ack 1 2)

(ack (- 1 1) 
     (ack (1 (- 2 1))))

 
(ack 0 2)
;Value: 4



(ack 1 1)
;Value: 2

(* 2 2 2 2 2 2 2 2 2 2)
;Value: 1024

(ack 2 4)
;Value: 65536


(ack (- 2 1) (ack 2 (- 4 1)))
;Value: 65536


(ack 1 (ack 2 3))
(ack 1 (ack 1 (ack 2 2)))
(ack 1 (ack 1 (ack 1 (ack 2 1))))
(ack 1 (ack 1 (ack 1 2)))
(ack 1 (ack 1 (ack 0 (ack 1 1))))








